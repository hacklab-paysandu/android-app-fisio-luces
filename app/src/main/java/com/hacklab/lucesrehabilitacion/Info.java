package com.hacklab.lucesrehabilitacion;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public class Info extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        findViewById( R.id.btnAtras ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Info.this.finish();
            }
        } );
    }
}
