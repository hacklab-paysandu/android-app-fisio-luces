package com.hacklab.lucesrehabilitacion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class Principal extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        findViewById( R.id.vNueva ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iniciar_pruebas();
            }
        } );

        findViewById( R.id.vVerRutinas ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ver_rutinas();
            }
        } );

        findViewById( R.id.vHistorial ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                historial();
            }
        } );

        findViewById( R.id.vInformacion ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sobre_proyecto();
            }
        } );

        findViewById( R.id.vSalir ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        } );
    }

    private void iniciar_pruebas ()
    {
        startActivity( new Intent( Principal.this, CrearRutina.class ) );
    }

    private void ver_rutinas()
    {
        startActivity( new Intent( Principal.this, VerRutinas.class ) );
    }

    private void sobre_proyecto ()
    {
        startActivity( new Intent( Principal.this, Info.class ) );
    }

    private void salir ()
    {
        new AlertDialog.Builder( this )
                .setIcon( android.R.drawable.ic_dialog_info )
                .setTitle( "Salir" )
                .setMessage( "¿Desea salir de la aplicación?" )
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Principal.this.finish();
                    }
                } )
                .setNegativeButton( "No", null )
                .show();
    }

    private void historial ()
    {
        startActivity( new Intent( Principal.this, Historial.class ) );
    }
}
