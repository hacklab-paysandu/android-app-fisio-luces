package com.hacklab.lucesrehabilitacion;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.hacklab.lucesrehabilitacion.AdaptadoresLista.HistorialAdapter;
import com.hacklab.lucesrehabilitacion.Clases.Hist;
import com.hacklab.lucesrehabilitacion.bd.BD;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Historial extends Activity
{
    // TODO: FIX
    private List<Hist> historial = null;
    private List<Hist> resultados = null;
    private String filtro = "";

    private View progressbar;
    private RecyclerView rv;
    private HistorialAdapter adapter;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historial);

        progressbar = findViewById( R.id.progressbar );

        rv = findViewById( R.id.rvHistoriales );

        findViewById( R.id.btnAtras ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Historial.this.finish();
            }
        } );

        findViewById( R.id.btnBorrarHist ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder( Historial.this )
                        .setIcon( android.R.drawable.ic_dialog_info )
                        .setTitle( "Borrar historial" )
                        .setMessage( "¿Desea borrar el historial de los ejercicios realizados?" )
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                BD.borrarHistorial( Historial.this );
                                historial = BD.listarHistorial( Historial.this );
                                cargar();

                                rv.setAdapter( new HistorialAdapter( Historial.this, resultados ) );
                                rv.setLayoutManager( new LinearLayoutManager( Historial.this ) );
                                Toast.makeText(Historial.this, "Historial borrado", Toast.LENGTH_SHORT).show();
                            }
                        } )
                        .setNegativeButton( "No", null )
                        .show();
            }
        } );

        ( ( EditText ) findViewById( R.id.etBusqueda ) ).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filtro = s.toString();
                cargar();

                rv.setAdapter( new HistorialAdapter( Historial.this, resultados ) );
                rv.setLayoutManager( new LinearLayoutManager( Historial.this ) );
            }
        });

        new AsyncTask<Void, Void, Void>()
        {
            @Override
            protected Void doInBackground ( Void... voids )
            {
                cargar();
                return null;
            }

            @Override
            protected void onPostExecute ( Void resultado )
            {
                adapter = new HistorialAdapter( Historial.this, resultados );
                rv.setAdapter( adapter );
                rv.setLayoutManager( new LinearLayoutManager( Historial.this ) );
                progressbar.setVisibility( View.GONE );
            }
        }.execute();
    }

    @Override
    protected void onResume ()
    {
        super.onResume();
        historial = BD.listarHistorial( this );
        cargar();

        adapter = new HistorialAdapter( Historial.this, resultados );
        rv.setAdapter( adapter );
        rv.setLayoutManager( new LinearLayoutManager( Historial.this ) );
        progressbar.setVisibility( View.GONE );
    }

    private void cargar ()
    {
        if ( historial == null )
            historial = BD.listarHistorial( this );

        List<String> nombres = new ArrayList<>();

        for ( Hist h : historial )
        {
            String nombre = h.getRutina();

            if ( !filtro.isEmpty() && !nombre.toLowerCase().contains( filtro.toLowerCase() ) )
                continue;

            boolean esta = false;
            for ( String n : nombres )
                if ( n.equals( nombre ) )
                {
                    esta = true;
                    break;
                }

            if ( !esta )
                nombres.add( nombre );
        }

        Collections.sort( nombres );

        resultados = new ArrayList<>();

        for ( String nombre : nombres )
            for ( Hist h : historial )
                if ( h.getRutina().equals( nombre ) )
                    resultados.add( h );
    }
}
