package com.hacklab.lucesrehabilitacion;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hacklab.lucesrehabilitacion.AdaptadoresLista.RutinasAdapter;
import com.hacklab.lucesrehabilitacion.Clases.Rutina;
import com.hacklab.lucesrehabilitacion.bd.BD;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VerRutinas extends Activity
{
    @BindView( R.id.rvLista )
    RecyclerView rvLista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_rutinas);
        ButterKnife.bind( this );

        cargar();

        findViewById( R.id.btnAtras ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VerRutinas.this.finish();
            }
        } );
    }

    @Override
    protected void onResume ()
    {
        super.onResume();
        cargar();
    }

    private void cargar ()
    {
        List<Rutina> rutinas = BD.listarRutinas( this );

        RutinasAdapter adaptador = new RutinasAdapter(this, rutinas );
        rvLista.setAdapter(adaptador);
        rvLista.setLayoutManager( new LinearLayoutManager( this ) );
    }
}
