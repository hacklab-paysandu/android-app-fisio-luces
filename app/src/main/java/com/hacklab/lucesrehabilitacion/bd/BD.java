package com.hacklab.lucesrehabilitacion.bd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.hacklab.lucesrehabilitacion.Clases.Ejercicio;
import com.hacklab.lucesrehabilitacion.Clases.Hist;
import com.hacklab.lucesrehabilitacion.Clases.Resultado;
import com.hacklab.lucesrehabilitacion.Clases.Rutina;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BD extends SQLiteOpenHelper
{
    private static final String BD_NOMBRE = "fisio.bd";
    private static final int BD_VERSION = 11;
    private static final boolean BD_RESTART = true;

    private static final String TAG = "BD";
    private static final boolean DEBUG = false;

    private BD( Context c ) {
        super(c, BD_NOMBRE, null, BD_VERSION);

        if ( DEBUG )
        {
            SQLiteDatabase db = getReadableDatabase();

            Log.e(TAG, "============================================");
            Log.e(TAG, "============================================");
            Log.e(TAG, "============================================");
            Log.e(TAG, "============================================");
            Log.e(TAG, "============================================");

            Cursor cursor = db.rawQuery("SELECT id, nombre FROM rutina", null);
            Log.e(TAG, "=========== RUTINAS " + cursor.getCount());
            while (cursor.moveToNext())
                Log.e(TAG, String.format("%d,%s",
                        cursor.getInt(cursor.getColumnIndex("id")),
                        cursor.getString(cursor.getColumnIndex("nombre"))
                ));
            cursor.close();

            Cursor cursor2 = db.rawQuery("SELECT id_rutina, tipo FROM ejercicio", null);
            Log.e(TAG, "=========== EJERCICIOS " + cursor2.getCount());
            while (cursor2.moveToNext())
                Log.e(TAG, String.format("%d,%s",
                        cursor2.getInt(cursor2.getColumnIndex("id_rutina")),
                        cursor2.getString(cursor2.getColumnIndex("tipo"))
                ));
            cursor2.close();

            Cursor cursor3 = db.rawQuery("SELECT id_rutina, id FROM historial", null);
            Log.e(TAG, "=========== HISTORIALES " + cursor3.getCount());
            while (cursor3.moveToNext())
                Log.e(TAG, String.format("%d,%d",
                        cursor3.getInt(cursor3.getColumnIndex("id_rutina")),
                        cursor3.getInt(cursor3.getColumnIndex("id"))
                ));
            cursor3.close();

            Cursor cursor4 = db.rawQuery("SELECT id_historial, tipo, i1, i2, i3, i4 FROM resultado", null);
            Log.e(TAG, "=========== RESULTADOS " + cursor4.getCount());
            while (cursor4.moveToNext())
                Log.e(TAG, String.format("%d,%s,%d,%d,%d,%d",
                        cursor4.getInt(cursor4.getColumnIndex("id_historial")),
                        cursor4.getString(cursor4.getColumnIndex("tipo")),
                        cursor4.getInt( cursor4.getColumnIndex( "i1" ) ),
                        cursor4.getInt( cursor4.getColumnIndex( "i2" ) ),
                        cursor4.getInt( cursor4.getColumnIndex( "i3" ) ),
                        cursor4.getInt( cursor4.getColumnIndex( "i4" ) )
                ));
            cursor4.close();

            db.close();
        }
    }

    @Override
    public void onCreate ( SQLiteDatabase db )
    {
        if ( DEBUG )
            Log.e( TAG, "onCreate" );

        db.execSQL( "CREATE TABLE IF NOT EXISTS rutina ( id INTEGER PRIMARY KEY AUTOINCREMENT, nombre VARCHAR( 30 ) )" );
        db.execSQL( "CREATE TABLE IF NOT EXISTS ejercicio ( tipo VARCHAR( 30 ), repeticiones INTEGER, id_rutina INTEGER )" );

        db.execSQL( "CREATE TABLE IF NOT EXISTS historial ( id INTEGER PRIMARY KEY AUTOINCREMENT, realizado VARCHAR( 20 ), id_rutina INTEGER )" );
        db.execSQL( "CREATE TABLE IF NOT EXISTS resultado ( tipo VARCHAR( 30 ), tiempo REAL, i1 TINYINT, i2 TINYINT, i3 TINYINT, i4 TINYINT, id_historial INTEGER )" );
    }

    @Override
    public void onUpgrade ( SQLiteDatabase db, int vieja, int nueva )
    {
        if ( DEBUG )
            Log.e( TAG, "onUpgrade" );

        if ( BD_RESTART )
        {
            db.execSQL( "DROP TABLE IF EXISTS rutina" );
            db.execSQL( "DROP TABLE IF EXISTS ejercicio" );

            db.execSQL( "DROP TABLE IF EXISTS historial" );
            db.execSQL( "DROP TABLE IF EXISTS resultado" );
        }
        else
        {
            db.delete("rutina", null, null);
            db.delete("ejercicio", null, null);

            db.delete("historial", null, null);
            db.delete("resultado", null, null);
        }

        onCreate( db );
    }

    private static long rutinaId ( SQLiteDatabase db, String nombre )
    {
        Cursor cursor = db.rawQuery( "SELECT id FROM rutina WHERE nombre = ?", new String[] { nombre } );
        cursor.moveToNext();
        int id_rutina = cursor.getInt( cursor.getColumnIndex( "id" ) );
        cursor.close();

        return  id_rutina;
    }

    public static void agregarRutina ( Context c, String nombre, List<Ejercicio> ejercicios )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put( "nombre", nombre );

        long id_rutina = db.insert( "rutina", null, valores );

        for ( Ejercicio e : ejercicios )
        {
            valores = new ContentValues();

            valores.put( "id_rutina", id_rutina );
            valores.put( "tipo", e.getTipo() );
            valores.put( "repeticiones", e.getRepeticiones() );

            db.insert( "ejercicio", null, valores );
        }

        db.close();
        base.close();
    }

    public static void modificarRutina (Context c, String nombre, List<Ejercicio> ejercicios )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        long id_rutina = rutinaId( db, nombre );

        db.delete( "ejercicio", "id_rutina = ?", new String[] { String.format( Locale.getDefault(), "%d", id_rutina ) } );

        for ( Ejercicio e : ejercicios )
        {
            ContentValues valores = new ContentValues();

            valores.put( "id_rutina", id_rutina );
            valores.put( "tipo", e.getTipo() );
            valores.put( "repeticiones", e.getRepeticiones() );

            db.insert( "ejercicio", null, valores );
        }

        db.close();
        base.close();
    }

    public static void eliminarRutina (Context c, String nombre )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        long id_rutina = rutinaId( db, nombre );

        db.delete( "rutina", "nombre = ?", new String[] { nombre } );
        db.delete( "ejercicio", "id_rutina = ?", new String[] { String.format( Locale.getDefault(), "%d", id_rutina ) } );

        db.close();
        base.close();
    }

    public static boolean existeRutina ( Context c, String nombre )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        Cursor cursor = db.rawQuery( "SELECT * FROM rutina WHERE nombre = ?", new String[] { nombre } );
        boolean res = cursor.getCount() != 0;

        cursor.close();
        db.close();
        base.close();

        return res;
    }

    public static List<Rutina> listarRutinas ( Context c )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        Cursor cursor = db.rawQuery( "SELECT nombre FROM rutina", null );

        List<Rutina> res = new ArrayList<> ();
        while ( cursor.moveToNext() )
            res.add( new Rutina( cursor.getString( cursor.getColumnIndex( "nombre" ) ) ) );

        cursor.close();
        db.close();
        base.close();

        return res;
    }

    public static List<Ejercicio> listarEjercicios ( Context c, String rutina )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        long id_rutina = rutinaId( db, rutina );

        Cursor cursor = db.rawQuery( "SELECT tipo, repeticiones FROM ejercicio WHERE id_rutina = ?", new String[] { String.format( Locale.getDefault(), "%d", id_rutina ) } );

        List<Ejercicio> res = new ArrayList<> ();
        while ( cursor.moveToNext() )
        {
            String tipo = cursor.getString( cursor.getColumnIndex( "tipo" ) );
            int repeticiones = cursor.getInt( cursor.getColumnIndex( "repeticiones" ) );

            res.add( new Ejercicio( tipo, repeticiones ) );
        }

        cursor.close();
        db.close();
        base.close();

        return res;
    }

    private static long historialId ( SQLiteDatabase db, long id_rutina, String realizado )
    {
        Cursor cursor = db.rawQuery( "SELECT id FROM historial WHERE id_rutina = ? AND realizado = ?", new String[] { String.format( Locale.getDefault(), "%d", id_rutina ), realizado } );
        cursor.moveToNext();
        int id_historial = cursor.getInt( cursor.getColumnIndex( "id" ) );
        cursor.close();

        return id_historial;
    }

    public static long agregarHistorial ( Context c, String rutina, String realizado, List<Resultado> resultados )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        long id_rutina = rutinaId( db, rutina );

        ContentValues valores = new ContentValues();
        valores.put( "realizado", realizado );
        valores.put( "id_rutina", id_rutina );

        long id_historial = db.insert( "historial", null, valores );

        for ( Resultado r : resultados )
        {
            valores = new ContentValues();

            valores.put( "id_historial", id_historial );
            valores.put( "tipo", r.getTipo() );
            valores.put( "tiempo", r.getTiempo() );
            valores.put( "i1", r.getIntento( 0 ) );
            valores.put( "i2", r.getIntento( 1 ) );
            valores.put( "i3", r.getIntento( 2 ) );
            valores.put( "i4", r.getIntento( 3 ) );

            db.insert( "resultado", null, valores );
        }

        db.close();
        base.close();

        return id_historial;
    }

    public static List<Hist> listarHistorial ( Context c )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        Cursor cursor = db.rawQuery( "SELECT h.id AS id, r.nombre AS nombre, h.realizado AS realizado FROM rutina AS r, historial AS h WHERE r.id = h.id_rutina", null );

        List<Hist> res = new ArrayList<> ();
        while ( cursor.moveToNext() )
        {
            int id = cursor.getInt( cursor.getColumnIndex( "id" ) );
            String nombre = cursor.getString( cursor.getColumnIndex( "nombre" ) );
            String realizado = cursor.getString( cursor.getColumnIndex( "realizado" ) );

            res.add( new Hist( id, nombre, realizado ) );
        }

        cursor.close();
        db.close();
        base.close();

        return res;
    }

    public static List<Resultado> listarResultados ( Context c, long id_historial )
    {
        Log.e( "QAZWSX", "listarResultados " + id_historial );
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        Cursor cursor = db.rawQuery( "SELECT tipo, tiempo, i1, i2, i3, i4 FROM resultado WHERE id_historial = ?", new String[] { String.format( Locale.getDefault(), "%d", id_historial ) } );

        Log.e( TAG, "listarResultados " + id_historial );

        List<Resultado> res = new ArrayList<> ();
        while ( cursor.moveToNext() )
        {
            String tipo = cursor.getString( cursor.getColumnIndex( "tipo" ) );
            float tiempo = cursor.getFloat( cursor.getColumnIndex( "tiempo" ) );
            boolean i1 = cursor.getInt( cursor.getColumnIndex( "i1" ) ) == 1;
            boolean i2 = cursor.getInt( cursor.getColumnIndex( "i2" ) ) == 1;
            boolean i3 = cursor.getInt( cursor.getColumnIndex( "i3" ) ) == 1;
            boolean i4 = cursor.getInt( cursor.getColumnIndex( "i4" ) ) == 1;

            res.add( new Resultado( tipo, tiempo, new boolean[] { i1, i2, i3, i4 } ) );
        }

        cursor.close();
        db.close();
        base.close();

        return res;
    }

    public static void borrarResultados ( Context c, String rutina, String realizado )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        long id_rutina = rutinaId( db, rutina );
        long id_historial = historialId( db, id_rutina, realizado );

        db.delete( "resultado", "id_historial = ?", new String[] { String.format( Locale.getDefault(), "%d", id_historial ) } );
        db.delete( "historial", "id_rutina = ? AND realizado = ?", new String[] { String.format( Locale.getDefault(), "%d", id_rutina ), realizado } );

        db.close();
        base.close();
    }

    public static void borrarHistorial ( Context c )
    {
        BD base = new BD( c );
        SQLiteDatabase db = base.getWritableDatabase();

        db.delete( "resultado", null, null );
        db.delete( "historial", null, null );

        db.close();
        base.close();
    }
}
