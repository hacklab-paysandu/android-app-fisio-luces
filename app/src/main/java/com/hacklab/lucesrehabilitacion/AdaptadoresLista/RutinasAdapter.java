package com.hacklab.lucesrehabilitacion.AdaptadoresLista;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hacklab.lucesrehabilitacion.Clases.Rutina;
import com.hacklab.lucesrehabilitacion.CrearRutina;
import com.hacklab.lucesrehabilitacion.EjecutarRutina;
import com.hacklab.lucesrehabilitacion.R;
import com.hacklab.lucesrehabilitacion.bd.BD;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RutinasAdapter extends RecyclerView.Adapter<RutinasAdapter.RutinaViewHolder>
{
    private Activity c;
    private List<Rutina> rutinas;

    public RutinasAdapter(Activity c, List<Rutina> rutinas )
    {
        this.c = c;
        this.rutinas = rutinas;
    }

    private void ejecutar ( String nombre )
    {
        Intent i = new Intent(c, EjecutarRutina.class);
        i.putExtra("rutina", nombre);
        c.startActivity(i);
    }

    private void quitar ( final int p )
    {
        new AlertDialog.Builder( c )
                .setIcon( android.R.drawable.ic_dialog_info )
                .setTitle( "Borrar rutina" )
                .setMessage( "¿Desea borrar la rutina " + rutinas.get( p ).getNombre() + "?" )
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick ( DialogInterface dialog, int which )
                    {
                        BD.eliminarRutina( c, rutinas.get( p ).getNombre() );

                        rutinas.remove( p );
                        notifyItemRemoved( p );

                        for ( int i = 0; i < rutinas.size(); i++ )
                            notifyItemChanged( i );
                    }
                } )
                .setNegativeButton( "No", null )
                .show();
    }

    @NonNull
    @Override
    public RutinasAdapter.RutinaViewHolder onCreateViewHolder ( @NonNull ViewGroup padre, int viewTipo )
    {
        View v = LayoutInflater.from( padre.getContext() ).inflate( R.layout.item_rutinas, padre, false );
        return new RutinasAdapter.RutinaViewHolder( v, this );
    }

    @Override
    public void onBindViewHolder ( @NonNull RutinasAdapter.RutinaViewHolder holder, int position )
    {
        holder.bindRutina( rutinas.get( position ), position );
    }

    @Override
    public int getItemCount ()
    {
        return rutinas.size();
    }

    class RutinaViewHolder extends RecyclerView.ViewHolder
    {
        @BindView( R.id.tvNombre )
        TextView tvNombre;
        @BindView( R.id.btnEjecutar )
        Button btnEjecutar;
        @BindView( R.id.btnModificar )
        Button btnModificar;
        @BindView( R.id.btnEliminar )
        Button btnEliminar;

        private Context c;
        private RutinasAdapter ra;

        RutinaViewHolder ( View itemView, RutinasAdapter ra )
        {
            super( itemView );
            this.ra = ra;
            ButterKnife.bind( this, itemView );

            this.c = itemView.getContext();
        }

        void bindRutina ( final Rutina r, final int position )
        {
            tvNombre.setText( r.getNombre() );

            btnEjecutar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick( View v )
                {
                    ra.ejecutar( r.getNombre() );
                }
            });
            
            btnModificar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick( View v )
                {
                    Intent i = new Intent( c, CrearRutina.class );
                    i.putExtra( "rutina", r.getNombre() );
                    c.startActivity( i );
                }
            });
            
            btnEliminar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick ( View v )
                {
                    ra.quitar( position );
                }
            });
        }
    }
}
