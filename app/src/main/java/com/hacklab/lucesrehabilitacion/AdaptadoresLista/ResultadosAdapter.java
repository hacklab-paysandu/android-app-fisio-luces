package com.hacklab.lucesrehabilitacion.AdaptadoresLista;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hacklab.lucesrehabilitacion.Clases.Resultado;
import com.hacklab.lucesrehabilitacion.R;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ResultadosAdapter extends RecyclerView.Adapter<ResultadosAdapter.ResultadoViewHolder>
{
    private List<Resultado> resultados;

    public ResultadosAdapter( List<Resultado> resultados )
    {
        this.resultados = resultados;
    }

    @NonNull
    @Override
    public ResultadosAdapter.ResultadoViewHolder onCreateViewHolder ( @NonNull ViewGroup padre, int viewTipo )
    {
        View v = LayoutInflater.from( padre.getContext() ).inflate( R.layout.item_resultado, padre, false );
        return new ResultadosAdapter.ResultadoViewHolder( v );
    }

    @Override
    public void onBindViewHolder ( @NonNull ResultadosAdapter.ResultadoViewHolder holder, int position )
    {
        holder.bindResultado( resultados.get( position ) );
    }

    @Override
    public int getItemCount ()
    {
        return resultados.size();
    }

    class ResultadoViewHolder extends RecyclerView.ViewHolder
    {
        @BindView( R.id.tvTitulo )
        TextView tvTitulo;
        @BindView( R.id.tvVelocidad )
        TextView tvVelocidad;
        @BindView( R.id.lOtro )
        LinearLayout lOtro;
        @BindView( R.id.iv1 )
        ImageView iv1;
        @BindView( R.id.iv2 )
        ImageView iv2;
        @BindView( R.id.iv3 )
        ImageView iv3;
        @BindView( R.id.iv4 )
        ImageView iv4;

        private ImageView[] ivs;

        ResultadoViewHolder ( View itemView )
        {
            super( itemView );
            ButterKnife.bind( this, itemView );

            ivs = new ImageView[] { iv1, iv2, iv3, iv4 };
        }

        void bindResultado (Resultado r )
        {
            Log.e( "ASDASD", r.toString() );
            tvTitulo.setText( r.getTipo() );

            if ( r.getTipo().equals( "Velocidad" ) || r.getTipo().equals( "Descanso" ) )
            {
                tvVelocidad.setText( String.format( Locale.getDefault(), "%.2f segundos", r.getTiempo() ) );

                lOtro.setVisibility( View.GONE );
                tvVelocidad.setVisibility( View.VISIBLE );
            }
            else
            {
                boolean[] intentos = r.getIntentos();

                if ( r.getTipo().equals( "Secuencia" ) )
                {
                    for ( int i = 0; i < intentos.length; i++ )
                        if ( intentos[i] )
                        {
                            ivs[i].setColorFilter( Color.parseColor( "#00FF00" ) );
                            ivs[i].setImageResource( R.drawable.ic_bien );
                        }
                        else
                        {
                            ivs[i].setColorFilter( Color.parseColor( "#FF0000" ) );
                            ivs[i].setImageResource( R.drawable.ic_mal );
                        }
                }

                if ( r.getTipo().equals( "Palabra" ) || r.getTipo().equals( "Color" ) )
                {
                    int i;
                    for ( i = 0; i < intentos.length; i++ )
                    {
                        if ( intentos[i] )
                        {
                            ivs[i].setColorFilter( Color.parseColor( "#00FF00" ) );
                            ivs[i].setImageResource( R.drawable.ic_bien );
                            break;
                        }
                        else
                        {
                            ivs[i].setColorFilter( Color.parseColor( "#FF0000" ) );
                            ivs[i].setImageResource( R.drawable.ic_mal );
                        }
                    }

                    Log.e( "OJOOOOOO", r.getTipo() + " " + i );
                    Log.e( "OJOOOOOO", "" + intentos[0] + " " + intentos[1] + " " + intentos[2] + " " + intentos[3] );

                    for ( int j = i + 1; j < intentos.length; j++ )
                    {
                        ivs[j].setImageResource(R.drawable.ic_mal);

                        if ( r.getTipo().equals( "Secuencia" ) )
                            ivs[j].setColorFilter(Color.parseColor("#FF0000"));
                        else
                            ivs[j].setVisibility( View.GONE );
                    }
                }

                tvVelocidad.setVisibility( View.GONE );
                lOtro.setVisibility( View.VISIBLE );
            }
        }
    }
}
