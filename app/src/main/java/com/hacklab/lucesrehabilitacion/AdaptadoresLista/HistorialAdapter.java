package com.hacklab.lucesrehabilitacion.AdaptadoresLista;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hacklab.lucesrehabilitacion.Clases.Hist;
import com.hacklab.lucesrehabilitacion.Historial;
import com.hacklab.lucesrehabilitacion.R;
import com.hacklab.lucesrehabilitacion.Resultados;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistorialAdapter extends RecyclerView.Adapter<HistorialAdapter.HistorialViewHolder>
{
    private Historial c;
    private List<Hist> historial;

    public HistorialAdapter(Historial c, List<Hist> historial )
    {
        this.c = c;
        this.historial = historial;
    }

    private void abrir ( Hist h )
    {
        Intent i = new Intent( c, Resultados.class );
        i.putExtra( "nombre", h.getRutina() + "," + h.getRealizado() );
        i.putExtra( "id", h.getId() );
        c.startActivity( i );
    }

    @NonNull
    @Override
    public HistorialAdapter.HistorialViewHolder onCreateViewHolder ( @NonNull ViewGroup padre, int viewTipo )
    {
        View v = LayoutInflater.from( padre.getContext() ).inflate( R.layout.item_historial, padre, false );
        return new HistorialAdapter.HistorialViewHolder( v, this );
    }

    @Override
    public void onBindViewHolder ( @NonNull HistorialAdapter.HistorialViewHolder holder, int position )
    {
        holder.bindHistorial( historial.get( position ) );
    }

    @Override
    public int getItemCount ()
    {
        return historial.size();
    }

    class HistorialViewHolder extends RecyclerView.ViewHolder
    {
        @BindView( R.id.tvNombre )
        TextView tvNombre;
        @BindView( R.id.tvFecha )
        TextView tvFecha;
        @BindView( R.id.btnEntrar )
        View btnEntrar;

        private HistorialAdapter ra;

        HistorialViewHolder ( View itemView, HistorialAdapter ra )
        {
            super( itemView );
            this.ra = ra;
            ButterKnife.bind( this, itemView );
        }

        void bindHistorial ( final Hist h )
        {
            tvNombre.setText( h.getRutina() );
            tvFecha.setText( h.getRealizado() );

            btnEntrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ra.abrir( h );
                }
            });
        }
    }
}
