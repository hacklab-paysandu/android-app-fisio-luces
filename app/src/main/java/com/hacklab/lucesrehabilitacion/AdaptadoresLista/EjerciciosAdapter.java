package com.hacklab.lucesrehabilitacion.AdaptadoresLista;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hacklab.lucesrehabilitacion.Clases.Ejercicio;
import com.hacklab.lucesrehabilitacion.R;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EjerciciosAdapter extends RecyclerView.Adapter<EjerciciosAdapter.EjercicioViewHolder>
{
    private List<Ejercicio> ejercicios;

    public EjerciciosAdapter( List<Ejercicio> ejercicios )
    {
        this.ejercicios = ejercicios;
    }

    public void agregar ( Ejercicio e )
    {
        ejercicios.add( e );
        notifyItemInserted( ejercicios.size() - 1 );

        for ( int i = 0; i < ejercicios.size(); i++ )
            notifyItemChanged( i );
    }

    private void quitar ( int p )
    {
        ejercicios.remove( p );
        notifyItemRemoved( p );

        for ( int i = 0; i < ejercicios.size(); i++ )
            notifyItemChanged( i );
    }

    private void subir ( int p )
    {
        Ejercicio temp = ejercicios.get( p );
        ejercicios.set( p, ejercicios.get( p - 1 ) );
        ejercicios.set( p - 1, temp );

        notifyItemChanged( p );
        notifyItemChanged( p - 1 );
        notifyItemMoved( p, p - 1 );
    }

    private void bajar ( int p )
    {
        Ejercicio temp = ejercicios.get( p );
        ejercicios.set( p, ejercicios.get( p + 1 ) );
        ejercicios.set( p + 1, temp );

        notifyItemChanged( p );
        notifyItemChanged( p + 1 );
        notifyItemMoved( p, p + 1 );
    }

    public List<Ejercicio> getEjercicios ()
    {
        return this.ejercicios;
    }

    @NonNull
    @Override
    public EjerciciosAdapter.EjercicioViewHolder onCreateViewHolder ( @NonNull ViewGroup padre, int viewTipo )
    {
        View v = LayoutInflater.from( padre.getContext() ).inflate( R.layout.item_ejercicios, padre, false );
        return new EjerciciosAdapter.EjercicioViewHolder( v, this );
    }

    @Override
    public void onBindViewHolder ( @NonNull EjerciciosAdapter.EjercicioViewHolder holder, int position )
    {
        holder.bindEjercicio( ejercicios.get( position ), position, ejercicios.size() );
    }

    @Override
    public int getItemCount ()
    {
        return ejercicios.size();
    }

    class EjercicioViewHolder extends RecyclerView.ViewHolder
    {
        @BindView( R.id.tvOrden )
        TextView tvOrden;
        @BindView( R.id.tvTipo )
        TextView tvTipo;
        @BindView( R.id.etCant )
        TextView tvCant;

        @BindView( R.id.btnSubir )
        Button btnSubir;
        @BindView( R.id.btnBajar )
        Button btnBajar;
        @BindView( R.id.btnQuitar )
        Button btnQuitar;

        private EjerciciosAdapter ea;

        EjercicioViewHolder ( View itemView, EjerciciosAdapter ea )
        {
            super( itemView );
            this.ea = ea;
            ButterKnife.bind( this, itemView );
        }

        void bindEjercicio ( Ejercicio e, final int position, int tam )
        {
            tvOrden.setText( String.format( Locale.getDefault(), "%d", position + 1 ) );
            tvTipo.setText( e.getTipo() );
            tvCant.setText( String.format( Locale.getDefault(), "%d", e.getRepeticiones() ) );

            if ( position == 0 )
                btnSubir.setEnabled( false );
            else
                btnSubir.setEnabled( true );

            if ( position == ( tam - 1 ) )
                btnBajar.setEnabled( false );
            else
                btnBajar.setEnabled( true );

            btnSubir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ea.subir( position );
                }
            });

            btnBajar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ea.bajar( position );
                }
            });

            btnQuitar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ea.quitar( position );
                }
            });
        }
    }
}
