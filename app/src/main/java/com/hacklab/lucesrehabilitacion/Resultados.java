package com.hacklab.lucesrehabilitacion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hacklab.lucesrehabilitacion.AdaptadoresLista.ResultadosAdapter;
import com.hacklab.lucesrehabilitacion.Clases.Resultado;
import com.hacklab.lucesrehabilitacion.bd.BD;

import java.util.List;

public class Resultados extends Activity
{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resultados);

        findViewById( R.id.btnAtras ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Resultados.this.finish();
            }
        } );

        List<Resultado> resultados;

        int id = getIntent().getIntExtra("id", -1 );
        long id_long = getIntent().getLongExtra( "id", -1 );

        Log.e( "QAZWSX", "Resultados " + id );
        String nombre = getIntent().getStringExtra("nombre" );

        final String nombreRutina = nombre.split( "," )[0];
        final String realizado = nombre.split( "," )[1];


        findViewById( R.id.scrollView ).post(new Runnable() {
            @Override
            public void run() {
                ( (ScrollView ) findViewById( R.id.scrollView ) ).fullScroll( View.FOCUS_UP );
            }
        });

        findViewById( R.id.btnBorrar ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder( Resultados.this )
                        .setIcon( android.R.drawable.ic_dialog_info )
                        .setTitle( "Borrar resultado" )
                        .setMessage( "¿Desea borrar este resultado?" )
                        .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick ( DialogInterface dialog, int which )
                            {
                                BD.borrarResultados( Resultados.this, nombreRutina, realizado );
                                Resultados.this.finish();
                            }
                        } )
                        .setNegativeButton( "No", null )
                        .show();
            }
        });

        RecyclerView rvRes = findViewById( R.id.rvResultados );

        ( ( TextView ) findViewById( R.id.tvNombreRutina ) ).setText( nombreRutina );
        ( ( TextView ) findViewById( R.id.tvRealizado ) ).setText( realizado );

        //resultados = BD.listarResultados( this, nombreRutina, realizado );
        resultados = BD.listarResultados( this, id == -1 ? id_long : id );

        rvRes.setAdapter( new ResultadosAdapter( resultados ) );
        rvRes.setLayoutManager( new LinearLayoutManager( this ) );
    }
}
