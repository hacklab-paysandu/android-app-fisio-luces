package com.hacklab.lucesrehabilitacion.Clases;

public class Ejercicio
{
    private String tipo;
    private int repeticiones;

    public Ejercicio ( String tipo, int repeticiones )
    {
        this.tipo = tipo;
        this.repeticiones = repeticiones;
    }

    public String getTipo ()
    {
        return tipo;
    }

    public int getRepeticiones ()
    {
        return repeticiones;
    }

    public void descontar ()
    {
        this.repeticiones--;
    }
}
