package com.hacklab.lucesrehabilitacion.Clases;

import android.support.annotation.NonNull;

public class Resultado
{
    private String tipo;
    private float tiempo;
    private boolean[] intentos;

    public Resultado ( String tipo )
    {
        this.tipo = tipo;
        this.intentos = new boolean[4];

        for ( int i = 0; i < 4; i++ )
            this.intentos[i] = false;
    }

    public Resultado ( String tipo, float tiempo, boolean[] intentos )
    {
        this.tipo = tipo;
        this.tiempo = tiempo;
        this.intentos = intentos;
    }

    public String getTipo ()
    {
        return tipo;
    }

    public float getTiempo ()
    {
        return tiempo;
    }

    public void setTiempo ( float tiempo )
    {
        this.tiempo = tiempo;
    }

    public void setIntento ( int p, boolean i )
    {
        this.intentos[p] = i;
    }

    public boolean getIntento ( int p )
    {
        return this.intentos[p];
    }

    public boolean[] getIntentos()
    {
        return this.intentos;
    }

    @NonNull
    public String toString ()
    {
        if ( tipo.equals( "Velocidad" ) || tipo.equals( "Descanso" ) )
            return tipo + "," + tiempo;
        else
            return tipo + "," + intentos[0] + "," + intentos[1] + "," + intentos[2] + "," + intentos[3];
    }
}
