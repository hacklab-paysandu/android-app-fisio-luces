package com.hacklab.lucesrehabilitacion.Clases;

public class Hist
{
    private int id;
    private String rutina;
    private String realizado;

    public Hist ( int id, String rutina, String realizado )
    {
        this.id = id;
        this.rutina = rutina;
        this.realizado = realizado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRutina() {
        return rutina;
    }

    public void setRutina(String rutina) {
        this.rutina = rutina;
    }

    public String getRealizado() {
        return realizado;
    }

    public void setRealizado(String realizado) {
        this.realizado = realizado;
    }
}
