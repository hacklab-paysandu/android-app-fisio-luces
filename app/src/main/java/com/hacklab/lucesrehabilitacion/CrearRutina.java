package com.hacklab.lucesrehabilitacion;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.hacklab.lucesrehabilitacion.AdaptadoresLista.EjerciciosAdapter;
import com.hacklab.lucesrehabilitacion.Clases.Ejercicio;
import com.hacklab.lucesrehabilitacion.bd.BD;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CrearRutina extends Activity
{
    @BindView( R.id.spnTipo )
    Spinner spnTipo;
    @BindView( R.id.etCant )
    EditText etCant;
    @BindView( R.id.rvLista )
    RecyclerView rvLista;
    @BindView( R.id.btnGuardar )
    View ivGuardar;
    @BindView( R.id.etNombreRutina )
    EditText etNombreRutina;

    private EjerciciosAdapter adaptador;

    private String nombreInicial = "";
    private boolean modificando = false;

    @Override
    protected void onCreate ( Bundle savedInstanceState )
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_rutina);
        ButterKnife.bind( this );

        List<String> tipos = new ArrayList<>();
        tipos.add( "Velocidad" );
        tipos.add( "Secuencia" );
        tipos.add( "Palabra" );
        tipos.add( "Color" );
        tipos.add( "Descanso" );

        ArrayAdapter<String> tiposAdapter = new ArrayAdapter<>( this, android.R.layout.simple_spinner_item, tipos );
        tiposAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        spnTipo.setAdapter( tiposAdapter );

        List<Ejercicio> ejercicios = new ArrayList<>();

        Bundle b = getIntent().getExtras();

        if ( b != null && !b.getString( "rutina", "" ).equals( "" ) )
        {
            nombreInicial = b.getString( "rutina" );
            etNombreRutina.setText( nombreInicial );

            ejercicios = BD.listarEjercicios( this, nombreInicial );

            modificando = true;
        }

        adaptador = new EjerciciosAdapter( ejercicios );
        rvLista.setAdapter( adaptador );
        rvLista.setLayoutManager( new LinearLayoutManager( this ) );

        findViewById( R.id.btnAgregar ).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adaptador.agregar( new Ejercicio( spnTipo.getSelectedItem().toString(), Integer.valueOf( etCant.getText().toString() ) ) );
            }
        });

        findViewById( R.id.btnAtras ).setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CrearRutina.this.finish();
            }
        } );
        
        ivGuardar.setOnClickListener( new View.OnClickListener ()
        {
            @Override
            public void onClick(View v) {
                final String nombre = etNombreRutina.getText().toString().trim();

                if ( nombre.equals( "" ) )
                    Toast.makeText(CrearRutina.this, "Ingrese un nombre de rutina", Toast.LENGTH_SHORT).show();
                else if ( BD.existeRutina( CrearRutina.this, nombre ) && !modificando )
                    Toast.makeText(CrearRutina.this, "Ya existe una rutina con el nombre " + nombre, Toast.LENGTH_SHORT).show();
                else if ( modificando )
                {
                    if ( nombreInicial.equals( nombre ) ) {
                        new AlertDialog.Builder(CrearRutina.this)
                                .setIcon(android.R.drawable.ic_dialog_info)
                                .setTitle("Modificar")
                                .setMessage("¿Desea guardar los cambios realizados a " + nombre + "?")
                                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        BD.modificarRutina( CrearRutina.this, nombre, adaptador.getEjercicios() );
                                        Toast.makeText(CrearRutina.this, "Rutina " + nombre + " actualizada", Toast.LENGTH_SHORT).show();
                                        finish();
                                    }
                                })
                                .setNegativeButton("No", null)
                                .show();
                    }
                    else if ( BD.existeRutina( CrearRutina.this, nombre ) )
                        Toast.makeText( CrearRutina.this, "Ya existe una rutina con el nombre " + nombre, Toast.LENGTH_SHORT).show();
                    else
                    {
                        BD.eliminarRutina( CrearRutina.this, nombreInicial );
                        BD.agregarRutina( CrearRutina.this, nombre, adaptador.getEjercicios() );
                        Toast.makeText(CrearRutina.this, "Rutina guardada como " + nombre + ".", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }
                else if ( adaptador.getEjercicios().size() == 0 )
                    Toast.makeText(CrearRutina.this, "Agrege algún ejercicio", Toast.LENGTH_SHORT).show();
                else
                {
                    BD.agregarRutina( CrearRutina.this, nombre, adaptador.getEjercicios() );
                    Toast.makeText(CrearRutina.this, "Rutina guardada como " + nombre + ".", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        } );
    }
}
