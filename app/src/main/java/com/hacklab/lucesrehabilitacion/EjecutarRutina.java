package com.hacklab.lucesrehabilitacion;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hacklab.lucesrehabilitacion.Clases.Ejercicio;
import com.hacklab.lucesrehabilitacion.Clases.Resultado;
import com.hacklab.lucesrehabilitacion.bd.BD;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.UUID;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EjecutarRutina extends Activity
{
    private static final String TAG = "BT";

    private static final String BT_NAME = "Fisio";
    private static final UUID BT_UUID = UUID.fromString( "00001101-0000-1000-8000-00805F9B34FB" );
    private static final int BT_ACTIVAR = 1;

    private BluetoothAdapter btAdapter = null;
    private BluetoothSocket btSocket = null;
    private boolean conexionEstablecida = false;

    private boolean receiverRegistrado = false;
    private final BroadcastReceiver receiver = new BroadcastReceiver ()
    {
        @Override
        public void onReceive ( Context context, Intent intent )
        {
            // Aquí llegan mensajes definidos en la función bt_conectar
            final String accion = intent.getAction();

            assert accion != null;
            if ( accion.equals( BluetoothDevice.ACTION_ACL_DISCONNECTED ) )
                // Si se desconectó un dispositivo con el cuál previamente se estableció conexión
                bt_dispositivo_desconectaro();
            else if ( accion.equals( BluetoothAdapter.ACTION_STATE_CHANGED ) )
            {
                // Si se cambió el estado de bluetooth
                final int estado = intent.getIntExtra( BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR );

                if ( estado == BluetoothAdapter.STATE_OFF )
                    // Si se cambió el estado y ahora está desconectado
                    bt_desconectado();
            }
        }
    };

    @BindView( R.id.flLoading )
    FrameLayout flLoading;
    @BindView( R.id.background )
    ImageView background;
    @BindView( R.id.tvCountDown )
    TextView tvCountDown;
    @BindView( R.id.tvTexto )
    TextView tvTexto;

    private List<Resultado> resultados;
    private List<Ejercicio> ejercicios;
    private String nombreRutina;
    private int[] colores;
    private String[] nombres;

    @Override
    protected void onCreate ( Bundle savedInstanceState )
    {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_ejecutar );

        ButterKnife.bind( this );

        // Se coloca la actividad en fullscreen
        getWindow().getDecorView().setSystemUiVisibility( View.SYSTEM_UI_FLAG_FULLSCREEN );

        // Se obtiene el nombre de la rutina que nos envía la actividad anterior
        nombreRutina = getIntent().getStringExtra( "rutina" );

        // Se obtiene la lista de ejercicios de esa rutina
        ejercicios = BD.listarEjercicios( this,nombreRutina );

        // Se obtienen los colores y nombres de colores que utiliza el módulo
        colores = new int[4];
        nombres = new String[4];

        colores[0] = get_color( 255, 0, 0 );
        colores[1] = get_color( 0, 255, 0 );
        colores[2] = get_color( 0, 0, 255 );
        colores[3] = get_color( 255, 255, 255 );

        nombres[0] = "rojo";
        nombres[1] = "verde";
        nombres[2] = "azul";
        nombres[3] = "blanco";

        resultados = new ArrayList<>();

        // Se obtiene el adaptador bluetooth
        btAdapter = BluetoothAdapter.getDefaultAdapter();

        if ( btAdapter == null )
        {
            // Si es null el dispositivo celular no cuenta con módulo bluetooth
            Toast.makeText( this, "Este dispositivo no cuenta con conexión bluetooth", Toast.LENGTH_SHORT ).show();
            finish(); // No se puede ejecutar el ejercicio, por esto, se cierra la actividad
        }
        else if ( btAdapter.isEnabled() )
            // Si bluetooth esta activado, se llama a conectar
            bt_conectar();
        else
            // De lo contrario se pide que active bluetooth
            new AlertDialog.Builder( this )
                .setIcon( android.R.drawable.ic_dialog_info )
                .setTitle( "Bluetooth" )
                .setMessage( "Es necesario encender el bluetooth" )
                .setPositiveButton("Ok", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick (DialogInterface dialog, int which )
                    {
                        // Se muestra en pantalla una petición para activar bluetooth, la respuesta llega a la función onActivityResult
                        startActivityForResult( new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE ), BT_ACTIVAR );
                    }
                } )
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialog) {
                        EjecutarRutina.this.finish();
                    }
                })
                .show();
    }

    private int get_color ( int col1, int col2, int col3 )
    {
        int r = ( col1 << 16 ) & 0x00FF0000;
        int g = ( col2 << 8  ) & 0x0000FF00;
        int b = ( col3       ) & 0x000000FF;

        return 0xFF000000 | r | g | b;
    }

    @Override
    protected void onActivityResult ( int requestCode, int resultCode, @Nullable Intent data )
    {
        super.onActivityResult( requestCode, resultCode, data );

        // Si requestCode es BT_ACTIVAR, entonces es el resultado que pedimos en onCreate de activación del bluetooth
        if ( requestCode == BT_ACTIVAR )
            if ( resultCode == RESULT_OK )
                // Si lo activó, se llama a conectar
                bt_conectar();
            else if ( resultCode == RESULT_CANCELED )
            {
                // Si canceló la activación del bluetooth, se especifica que es necesario y se cierra la actividad
                Toast.makeText( this, "Es necesario activar bluetooth", Toast.LENGTH_SHORT ).show();
                finish();
            }
    }

    @SuppressLint("StaticFieldLeak")
    private void bt_conectar ()
    {
        Log.e( TAG, "Conectando..." );

        // Se registra un receiver al cual llegarán mensajes que indican si se desconectó un dispositivo
        // o si se cambió el estado de bluetooth a desconectado o conectado
        registerReceiver( receiver, new IntentFilter( BluetoothDevice.ACTION_ACL_DISCONNECTED ) );
        registerReceiver( receiver, new IntentFilter( BluetoothAdapter.ACTION_STATE_CHANGED ) );
        receiverRegistrado = true;

        Set<BluetoothDevice> dispositivos = btAdapter.getBondedDevices();

        if ( dispositivos.size() == 0 )
        {
            // Si no hay dispositivos sincronizados en el bluetooth del celular
            Log.e( TAG, "Dispositivo no sincronizado ninguno" );
            Toast.makeText( this, "Dispositivo " + BT_NAME + " no sincronizado", Toast.LENGTH_SHORT ).show();
            finish();
        }
        else
        {
            BluetoothDevice dispositivo = null;

            // Se recorren los dispositivos y se guarda el de nombre BT_NAME
            for ( BluetoothDevice disp : dispositivos )
                if ( disp.getName().equals( BT_NAME ) )
                {
                    dispositivo = disp;
                    break;
                }

            // Si no se encontró el dispositivo de nombre BT_NAME
            if ( dispositivo == null )
            {
                Log.e( TAG, "Dispositivo no sincronizado" );
                Toast.makeText( this, "Dispositivo " + BT_NAME + " no sincronizado", Toast.LENGTH_SHORT ).show();
                finish();
            }
            else
            {
                // Si se encontró el dispositivo
                new AsyncTask<BluetoothDevice, Void, Void> ()
                {
                    // Esta clase se llama con new ConectarBT().execute()
                    // Lo que hace es ejecutar onPreExecute, doInBackground (en segundo plano), y onPostExecute, en ese orden
                    private boolean conectado = true;

                    @Override
                    protected void onPreExecute ()
                    {
                        Log.e( TAG, "onPreExecute" );
                    }

                    @Override
                    protected Void doInBackground ( BluetoothDevice... dispositivo )
                    {
                        Log.e( TAG, "doInBackground" );

                        // Se establece la conexión con el dispositivo bluetooth
                        // pasado en new ConectarBT().execute( dispositivo )
                        try
                        {
                            btSocket = dispositivo[0].createInsecureRfcommSocketToServiceRecord( BT_UUID );
                            btAdapter.cancelDiscovery();
                            btSocket.connect();
                        }
                        catch ( IOException e ) { conectado = false; }

                        return null;
                    }

                    @Override
                    protected void onPostExecute ( Void resultado )
                    {
                        super.onPostExecute( resultado );
                        Log.e( TAG, "onPostExecute" );

                        if ( !conectado )
                        {
                            // Si no se logró establecer la conexión se avisa y se cierra la actividad
                            Log.e( TAG, "No se pudo establecer la conexión" );
                            Toast.makeText( EjecutarRutina.this, "No se pudo establecer la conexión con " + BT_NAME, Toast.LENGTH_SHORT ).show();
                            finish();
                        }
                        else
                            // Si se logró establecer la conexión
                            conexion_establecida();
                    }
                }.execute( dispositivo );
            }
        }
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler()
    {
        @Override
        public void handleMessage ( Message msg )
        {
            if ( msg.what == 123 )
                bt_recibido( Objects.requireNonNull( msg.getData().getString( "msj" ) ) );
        }
    };

    @SuppressLint("StaticFieldLeak")
    private void conexion_establecida ()
    {
        Log.e( TAG, "Conexión establecida" );

        flLoading.setVisibility( View.GONE );
        background.setVisibility( View.VISIBLE );
        conexionEstablecida = true;

        // Al establecer una conexión se crea una nueva tarea asincrona donde se procesarán los mensajes recibidos del dispositivo bluetooth
        new AsyncTask<Void, Void, Void> ()
        {
            @Override
            protected Void doInBackground ( Void... dispositivo )
            {
                try
                {
                    // Se obtiene el objeto InputStream, este contiene información de mensajes de entrada
                    InputStream is = btSocket.getInputStream();

                    // El mensaje que se quiere recibir, comienza vacío
                    StringBuilder mensaje = new StringBuilder();

                    // Mientras haya una conexión establecida
                    while ( conexionEstablecida  )
                    {
                        if ( is.available() > 0 )
                        {
                            // Si hay algo dentro del stream

                            // Se obtiene el caracter
                            char c = ( char ) is.read();

                            if ( c == '\n' )
                            {
                                // Si el caracter es un enter, significa que ya se leyó el mensaje entero

                                // Se manda el mensaje a la función que lo procese
                                Message m3 = new Message();
                                m3.what = 123;
                                Bundle b = new Bundle();
                                b.putString( "msj", mensaje.toString() );
                                m3.setData( b );
                                handler.sendMessage( m3 );

                                // Se borra el mensaje anterior para que se acumule el siguiente
                                mensaje.setLength( 0 );
                            }
                            else
                                // Mientras no se reciba un enter, se acumulan los caracteres del mensaje
                                mensaje.append( c );
                        }
                    }
                }
                catch ( IOException e )
                {
                    // Cualquier error se desconecta el bluetooth
                    bt_desconectar();
                }

                return null;
            }
        }.execute();

        // Se llama al primer ejercicio
        siguienteEjercicio();
    }

    private int i = 0;
    private int previo = 0;

    private void bt_recibido ( String mensaje )
    {
        Log.e( TAG, "Mensaje recibido: " + mensaje );

        if ( mensaje.contains( "fin" ) )
        {
            // Si se recibe el mensaje fin entonces se acabó el ejercicio

            // Se obtiene el tipo del ejercicio desde los resultados
            String tipo = resultados.get( resultados.size() - 1 ).getTipo();

            // Si es de tipo velocidad, entonces se setea el tiempo en base al último tiempo que se recibió
            // esto hace que el ejercicio Velocidad tenga como resultado cuanto demoró en apagarlos a todos
            if ( tipo.equals( "Velocidad" ) )
                resultados.get( resultados.size() - 1 ).setTiempo( ( float ) previo / 1000 );

            // Se resetean variables
            i = previo = 0;

            // Se llama al siguiente ejercicio
            siguienteEjercicio();
        }
        else
        {
            // Si el mensaje no era fin, entonces es un resultado

            // todos los mensajes que llegan tienen el siguiente formato
            // arg1:arg2:arg3:arg4
            // El arg4 sólo viene en los que no son ejercicios
            String[] partes = mensaje.trim().split( ":" );

            if ( partes.length == 4 )
            {
                // Todos los ejercicios menos el de velocidad funcionan enviando un último parámetro
                // el cual es una 'c', si fue correcto o una 'i' si fue incorrecto

                // Se toma el último resultado de la lista y se coloca el intento (1, 2, 3 ó 4) a true o false
                // dependiendo de si se recibió una 'c' o una 'i'
                resultados.get( resultados.size() - 1 ).setIntento( i, partes[3].contains( "c" ) );
                i++;
            }
            else
                // Si no hay cuatro argumentos, entonces fue un resultado del ejercicio velocidad
                // se guarda el tiempo en previo
                previo = Integer.valueOf( partes[2] );
        }
    }

    private void siguienteEjercicio ()
    {
        if ( ejercicios.size() == 0 )
        {
            // Si ejercicios está vacío entonces ya se terminó la rutina

            // Se guardan los resultados de los ejercicios
            long id_historial = BD.agregarHistorial( EjecutarRutina.this, nombreRutina, getFecha(), resultados );

            Log.e( "QAZWSX", "siguienteEjercicio " + id_historial );

            // Se pasan los resultados a una pantalla para que los muestre
            Intent i = new Intent( EjecutarRutina.this, Resultados.class );
            i.putExtra( "nombre", nombreRutina + "," + getFecha() );
            i.putExtra( "id", id_historial );

            // Se inicia la actividad de mostrar resultados
            startActivity( i );
            // Si ya no hay más ejercicios se desconecta el bluetooth
            bt_desconectar();
            return;
        }

        try { Thread.sleep( 500 ); }
        catch ( InterruptedException ignored ) {}

        // Se obtiene el primer ejercicio de la lista
        Ejercicio e = ejercicios.get( 0 );

        // Se agrega un resultado del tipo de este ejercicio
        resultados.add( new Resultado( e.getTipo() ) );

        if ( e.getTipo().equals( "Descanso" ) )
            resultados.get( resultados.size() - 1 ).setTiempo( e.getRepeticiones() );

        // Se setea la ventana del ejercicio con valores por defecto

        // Se coloca el nombre del modo
        tvTexto.setText( e.getTipo() );
        // Por defecto letra tamaño 50
        tvTexto.setTextSize( TypedValue.COMPLEX_UNIT_SP, 50 );
        // Se cambia el color de fondo al por defecto
        background.setBackgroundColor( getResources().getColor( R.color.colorFondo ) );
        tvCountDown.setVisibility( View.GONE );
        tvTexto.setVisibility( View.VISIBLE );
        tvTexto.setTextColor( getResources().getColor( R.color.black ) );

        // Se modifica la ventana de ejercico en base a que modo se este ejecutando
        switch ( e.getTipo() )
        {
            case "Velocidad" :
                // Modo de velocidad, solo hace falta indicar al dispositivo
                bt_enviar( "modo:todos_i\n" );
                break;
            case "Secuencia" :
                // Modo de velocidad, solo hace falta indicar al dispositivo
                bt_enviar( "modo:sec\n" );
                break;
            case "Palabra" :
                // Modo palabra, se agranda el tamaño de la palabra para que se vea mejor
                tvTexto.setTextSize( TypedValue.COMPLEX_UNIT_SP, 150 );

                // Se selecciona un color aleatorio
                int col = new Random().nextInt( 4 );

                // Se selecciona un fondo aleatorio que no tenga el mismo índice que el color
                int fondo = new Random().nextInt( 4 );
                while ( fondo == col )
                    fondo = new Random().nextInt( 4 );

                // Se coloca el color de fondo, incorrecto en ejercicio
                background.setBackgroundColor( colores[fondo] );
                // Se coloca el texto de la letra para que diga el color de fondo, incorrecto en ejercicio
                tvTexto.setText( nombres[fondo].toUpperCase() );

                // Se coloca el color de la letra, este es el color correcto del ejercicio
                tvTexto.setTextColor( colores[col] );

                // Se indica al dispositivo que incio el ejercicio y qué color es el correcto
                bt_enviar( "modo:color:" + col + "\n" );
                break;
            case "Color" :
                // Para el color se oculta el texto
                tvTexto.setVisibility( View.GONE );

                // Se selecciona un color aleatorio
                int col2 = new Random().nextInt( 4 );

                // Se coloca ese color aleatorio de fondo
                background.setBackgroundColor( colores[col2] );

                // Se indica al dispositivo que incio el ejercicio y qué color es el correcto
                bt_enviar( "modo:color:" + col2 + "\n" );
                break;
            case "Descanso" :
                // Se setea el tiempo inicial en el texto de la cuenta regresiva
                tvCountDown.setText( String.format( Locale.getDefault(), "%d.00 s", e.getRepeticiones() ) );

                // Se oculta el texto normal
                tvTexto.setVisibility( View.GONE );
                // Se muestra el texto de la cuenta regresiva
                tvCountDown.setVisibility( View.VISIBLE );

                // Se inicia una cuenta regresiva que llama a la funcion onTick cada 10 milisegundos
                // y a la funcion onFinish cuando pase la cantidad de tiempo necesaria
                new CountDownTimer( e.getRepeticiones() * 1000, 10 )
                {
                    @Override
                    public void onTick ( long millisUntilFinished )
                    {
                        // Esto se llama cada 10 milisegundos y actualiza el texto en pantalla
                        tvCountDown.setText( String.format( Locale.getDefault(), "%.2f s", ( float ) millisUntilFinished / 1000 ) );
                    }

                    @Override
                    public void onFinish ()
                    {
                        // Al finalizar la cuenta regresiva

                        // Se quita el 'ejercicio' Descanso de la lista
                        ejercicios.remove( 0 );

                        // Se llama al siguiente ejercicio
                        siguienteEjercicio();
                    }
                }.start();
                break;
        }

        // Si el ejercicio es descanso, el mismo se va a eliminar cuando termine la cuenta regresiva, por lo que no se hace este paso
        if ( !e.getTipo().equals( "Descanso" ) )
        {
            // Si no es descanso

            // Se pregunta cuantas repeticiones quedan
            if ( e.getRepeticiones() == 1 )
                // Si queda solo una, esa es la que se está ejecutando, entonces se remueve el ejercicio
                ejercicios.remove( 0 );
            else
                // Si quedan mas de una repeticion, se descuenta una
                ejercicios.get( 0 ).descontar();
        }
    }

    private String getFecha ()
    {
        Calendar c = Calendar.getInstance();

        int min = c.get( Calendar.MINUTE );
        String minuto = ( min <= 9 ? "0" : "" ) + min;

        return String.format( Locale.getDefault(), "%d:%s %d-%d-%d",
                c.get( Calendar.HOUR_OF_DAY  ),
                minuto,
                c.get( Calendar.DAY_OF_MONTH ),
                c.get( Calendar.MONTH ) + 1,
                c.get( Calendar.YEAR )
        );
    }

    private void bt_enviar ( String mensaje )
    {
        Log.e( TAG, "Enviar mensaje: " + mensaje );

        try
        {
            if ( btSocket != null )
                // Si aún hay conexión se envía el mensaje
                btSocket.getOutputStream().write( mensaje.getBytes() );
            else
                Log.e( TAG, "Envio fallo" );
        }
        catch ( IOException e ) { Log.e( TAG, "Envio fallo, excepción" ); e.printStackTrace(); }
    }

    private void bt_desconectar ()
    {
        Log.e( TAG, "Desconectando..." );

        bt_enviar( "fin\n" );

        // Conexión establecida, para que el AsyncTask en conexion_establecida salga de su loop
        conexionEstablecida = false;

        if ( btSocket != null )
        {
            // Si no es null el socket
            Log.e( TAG, "Cerrando socket" );

            // Se cierra su conexión
            try { btSocket.close(); }
            catch ( IOException ignored ) {}
        }

        // Se cierra la actividad
        finish();
    }

    private void bt_dispositivo_desconectaro ()
    {
        Log.e( TAG, "Dispositivo desconectado" );
        Toast.makeText( this, "Dispositivo desconectado", Toast.LENGTH_SHORT ).show();
        bt_desconectar();
    }

    private void bt_desconectado ()
    {
        Log.e( TAG, "Bluetooth estado OFF" );
        Toast.makeText( this, "Bluetooth desconectado", Toast.LENGTH_SHORT ).show();
        bt_desconectar();
    }

    @Override
    protected void onRestart ()
    {
        super.onRestart();
        Log.e( TAG, "onRestart" );

        // Si se reinicia la actividad en medio de un ejercicio, se indica un error y se cierra la actividad
        Toast.makeText( this, "No se puedo completar el ejercicio", Toast.LENGTH_SHORT ).show();
        finish(); // Esto ocaciona que se llame a onDestroy
    }

    @Override
    protected void onDestroy ()
    {
        super.onDestroy();
        Log.e( TAG, "onDestroy" );

        // Al destruir la actividad (cerrarse), se quita el receiver de mensajes de bluetooth, de haber sido registrado
        if ( receiverRegistrado ) unregisterReceiver( receiver );
        // Se desconecta el bluetooth
        bt_desconectar();
    }
}
